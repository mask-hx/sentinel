package com.mask.demo.controller;

import com.mask.demo.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author hx
 * @since 2021/11/11 9:31 下午
 */

@RestController
public class FlowLimitController {
    private static final Logger log = LoggerFactory.getLogger(FlowLimitController.class);

    @Autowired
    private TestService testService;

    @GetMapping("/testA")
    public String testA(){
        log.info(Thread.currentThread().getName()+":testA");
        return "-----TestA";
    }

    @GetMapping("/testB")
    public String testB(){
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "-----TestB";
    }

    @GetMapping("/testC")
    public String testC(){
        return testService.common();
    }

    @GetMapping("/testD")
    public String testD(){
        return testService.common();
    }
}
