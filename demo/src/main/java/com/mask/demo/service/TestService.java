package com.mask.demo.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;

/**
 * @author hx
 * @since 2021/11/11 9:52 下午
 */

@Service
public class TestService {

    /**
     * 定义限流资源
     * @return info
     */
    @SentinelResource("common")
    public String common(){
        return "common";
    }
}
